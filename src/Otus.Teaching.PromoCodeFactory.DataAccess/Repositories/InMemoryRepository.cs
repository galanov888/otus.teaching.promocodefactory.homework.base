﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T value)
        {
            return Task.Run(() => Data.ToImmutableList().Add(value));
        }

        public Task UpdateAsync(T value)
        {
            return Task.Run(() =>
            {
                T removeValue = Data.Where(v => v.Id == value.Id).FirstOrDefault();
                if (removeValue != null)
                {
                    Data.ToImmutableList().Remove(removeValue);
                }
                Data.ToImmutableList().Add(value);
            });
        }

        public Task DeleteAsync(Guid id)
        {
            return Task.Run(() => {
                T deleteEmployee = Data.FirstOrDefault(v => v.Id == id);
                if (deleteEmployee == null)
                {
                    return;
                }
                Data.ToImmutableList().Remove(deleteEmployee);
            });
        }
    }
}