﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавление сотрудника
        /// </summary>
        /// <param name="employee">новый сотрудник</param>
        /// <returns></returns>
        [HttpPost("{employee:employee}")]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync(Employee employee)
        {
            await _employeeRepository.AddAsync(employee);

            var newEmployee = await _employeeRepository.GetByIdAsync(employee.Id);

            if (newEmployee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = newEmployee.Id,
                Email = newEmployee.Email,
                Roles = newEmployee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = newEmployee.FullName,
                AppliedPromocodesCount = newEmployee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Обновление сотрудника
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        [HttpPut("{employee:employee}")]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync(Employee employee)
        {
            await _employeeRepository.AddAsync(employee);

            var newEmployee = await _employeeRepository.GetByIdAsync(employee.Id);

            if (newEmployee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = newEmployee.Id,
                Email = newEmployee.Email,
                Roles = newEmployee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = newEmployee.FullName,
                AppliedPromocodesCount = newEmployee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удаление сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("id:guid")]
        public async Task<ActionResult<bool>> DeleteEmployeeByIdAsync(Guid id)
        {
            await _employeeRepository.DeleteAsync(id);
            var deleteEmployee = await _employeeRepository.GetByIdAsync(id);

            if (deleteEmployee == null)
                return true;
            return false;
        }
    }
}